import pandas as pd 
import re, csv
import mxnet as mx 
import numpy as np 
from mxnet import nd
from mxnet.contrib.text import embedding, vocab
import boto3
from collections import Counter
from itertools import islice, chain
import os
import pickle

s3 = boto3.resource('s3')
data_ctx = mx.cpu(1)
np.random.seed(123)

if os.path.isfile("coded_sentences.json"):
	sentences = pd.read_json("coded_sentences.json",orient='values')
	sentences = sentences.drop_duplicates(subset=1, inplace=False)
else:
	s3.meta.client.download_file("novetta-quote-extract", "sentences/coded_sentences.json", "coded_sentences.json")
	sentences = pd.read_json("coded_sentences.json",orient='values')
	sentences = sentences.drop_duplicates(subset=1, inplace=False)

pattern_delete = r"\|\d*\|"
pd_filter = sentences[1].str.contains(pattern_delete)
sentences = sentences[~pd_filter]
mxnet_sentences = pd.DataFrame()
mxnet_sentences["label"] = sentences.iloc[:,2]
mxnet_sentences["sentence"] = sentences.iloc[:,1]
mxnet_sentences.loc[:]["sentence"] = " " + mxnet_sentences.loc[:,"sentence"]
mxnet_sentences["sentence"] = mxnet_sentences["sentence"].map(lambda x: re.sub(r'(["\'.,:!?()\]\[])', r' \1 ', x))
mxnet_sentences["sentence"] = mxnet_sentences["sentence"].map(lambda x: re.sub(r'([-\/\/\_\�])', r'', x))
mxnet_sentences = mxnet_sentences.sample(frac=1).reset_index(drop=True)
print("We have %d examples in our dataset" % len(mxnet_sentences))
mxnet_sentences[:340000].to_csv("train", header=False, index=False,  sep=" ", encoding='utf-8')
mxnet_sentences[340000:].to_csv("test", header=False, index=False, sep=" ", encoding='utf-8')
del mxnet_sentences
del sentences

def create_vocab(sentences, min_count=5, num_words = 100000):
    BOS_SYMBOL = "<s>"
    EOS_SYMBOL = "</s>"
    UNK_SYMBOL = "<unk>"
    PAD_SYMBOL = "<pad>"
    PAD_ID = 0
    TOKEN_SEPARATOR = " "
    VOCAB_SYMBOLS = [PAD_SYMBOL, UNK_SYMBOL, BOS_SYMBOL, EOS_SYMBOL]
    VOCAB_ENCODING = "utf-8"
    vocab_symbols_set = set(VOCAB_SYMBOLS)
    raw_vocab = Counter(token for line in sentences for token in line)
    return_vocab = vocab.Vocabulary(raw_vocab, min_freq = min_count, unknown_token = '<unk>')
    return return_vocab

def get_dataset(filename):
    labels = []
    sentences = []
    max_length = -1
    with open(filename) as f:
        for line in f:
            tokens = line.split()
            label = int(tokens[0])
            words = tokens[1:]
            max_length = max(max_length, len(words))
            labels.append(label)
            sentences.append(words)
    return sentences, labels, max_length

train_sentences, train_labels, _ = get_dataset('train')
test_sentences, test_labels, _ = get_dataset('test')

print(train_sentences[99]) # Just to check what's going on
print(train_labels[99])

# vocab is a Vocabulary object that serves as an index of individual words in the corpus of training sentences.
my_vocab = create_vocab(train_sentences) 
vocab_size = len(my_vocab)
print("There are %d words in the vocabulary." % vocab_size)

embedding_matrix = embedding.GloVe(pretrained_file_name = 'glove.6B.50d.txt', vocabulary = my_vocab)
EMBED_DIM = embedding_matrix.vec_len
print("Each word vector is %d - dimensional." % EMBED_DIM)

test_tokens = ["i", "live", "in", "karachi", "he", "said"]
#print(embedding_matrix.get_vecs_by_tokens(test_tokens)[1])
### Note that "karachi" is missing

## Creating the embedded data, which is an array of [sentence_embeddings]
## where sentence_embeddings is an array of shape (len(sentence), 100)

def create_embed(sents, labs):
	labels = nd.array(labs, ctx = data_ctx)
	print("created labels with no problem")
	MAX_LEN = 25
	
	embedded = nd.zeros((len(labels), MAX_LEN, EMBED_DIM), ctx = data_ctx)

	for i, row in enumerate(sents): 
	    if len(row) > MAX_LEN:
	        row = row[:MAX_LEN]
	    hold = embedding_matrix.get_vecs_by_tokens(row).as_in_context(data_ctx)
	    if i % 10000 == 0:
	    	print("The shape of hold is: " + str(hold.shape))
	    if len(hold) < MAX_LEN:
	    	fill = MAX_LEN - len(hold)
	    	fill_zeros = nd.zeros((fill, EMBED_DIM))
	    	embedded[i] = nd.concat(hold, fill_zeros, dim = 0)
	    else:
	    	embedded[i] = hold
	return labels, embedded

train_labs, train_sent_embed = create_embed(train_sentences, train_labels)
pickle.dump(train_labs, open("train_labs.p", "wb"), -1)
del train_labs
pickle.dump(train_sent_embed, open("train_sents.p", "wb"), -1)
del train_sent_embed

test_labs, test_sent_embed = create_embed(test_sentences, test_labels)
pickle.dump(test_labs, open("test_labs.p", "wb"), -1)
del test_labs
pickle.dump(test_sent_embed, open("test_sents.p", "wb"), -1)
del test_sent_embed