from __future__ import print_function

import logging
import mxnet as mx
from mxnet import gluon, autograd, nd
from mxnet.gluon import nn
print(mx.__version__)

import numpy as np
import json
import time
import re
from mxnet.io import NDArrayIter
import bisect, random
from collections import Counter
from itertools import chain, islice
import os
import boto3
import pickle

#from sklearn.metrics import precision_recall_fscore_support
#from sklearn import metrics

s3 = boto3.resource('s3')
logging.basicConfig(level=logging.DEBUG)

MAX_SENTENCE_LENGTH = 25

# ------------------------------------------------------------ #
# Training methods                                             #
# ------------------------------------------------------------ #


# A Simple Confusion Matrix Implementation
def confusionmatrix(actual, predicted, normalize = False):
    """
    Generate a confusion matrix for multiple classification
    @params:
        actual      - a list of integers or strings for known classes
        predicted   - a list of integers or strings for predicted classes
        normalize   - optional boolean for matrix normalization
    @return:
        matrix      - a 2-dimensional list of pairwise counts
    """
    unique = sorted(set(actual))
    matrix = [[0 for _ in unique] for _ in unique]
    imap   = {key: i for i, key in enumerate(unique)}
    # Generate Confusion Matrix
    for p, a in zip(predicted, actual):
        matrix[imap[p]][imap[a]] += 1
    # Matrix Normalization
    if normalize:
        sigma = sum([sum(matrix[imap[i]]) for i in unique])
        matrix = [row for row in map(lambda i: list(map(lambda j: j / sigma, i)), matrix)]
    return matrix

def train(current_host, hosts, num_cpus, num_gpus, channel_input_dirs, model_dir, hyperparameters, **kwargs):
    # retrieve the hyperparameters we set in notebook (with some defaults)
    batch_size = hyperparameters.get('batch_size', 8)
    epochs = hyperparameters.get('epochs', 2)
    learning_rate = hyperparameters.get('learning_rate', 0.01)
    log_interval = hyperparameters.get('log_interval', 1000)
    
    if len(hosts) == 1:
        kvstore = 'device' if num_gpus > 0 else 'local'
    else:
        kvstore = 'dist_device_sync' if num_gpus > 0 else 'dist_sync'

    ctx = mx.gpu() if num_gpus > 0 else mx.cpu()

    ### Fetch and load training data
    train_sentences, val_sentences = get_dataset("train_sents.p", ctx)
    train_labels, val_labels = get_dataset("train_labs.p", ctx)
    
    train_iterator = NDArrayIter(data = train_sentences, label = train_labels, batch_size = batch_size, shuffle = True)
    val_iterator = NDArrayIter(data = val_sentences, label = val_labels, batch_size = batch_size, shuffle = True)
    
    # define the network
    net = gluon.nn.Sequential()
    with net.name_scope():
        net.add(gluon.nn.Conv1D(channels = 64, kernel_size = 3, activation = 'relu', padding = 3))
        net.add(gluon.nn.MaxPool1D(3))
        net.add(gluon.nn.Conv1D(channels = 256, kernel_size = 7, activation = 'relu', padding = 7))
        net.add(gluon.nn.MaxPool1D(5))
        net.add(gluon.nn.Conv1D(channels = 128, kernel_size = 5, activation = 'relu', padding = 5))
        net.add(gluon.nn.MaxPool1D(5))
        net.add(gluon.nn.Conv1D(channels = 128, kernel_size = 5, activation = 'relu', padding = 5))
        net.add(gluon.nn.MaxPool1D(5))
        net.add(gluon.nn.Conv1D(channels = 256, kernel_size = 5, activation = 'relu', padding = 5))
        net.add(gluon.nn.GlobalMaxPool1D())
        net.add(gluon.nn.Flatten())
        net.add(gluon.nn.Dense(2, activation = 'softrelu'))

    # Collect all parameters from net and its children, then initialize them.
    net.collect_params().initialize(mx.init.Xavier(magnitude=2.24), ctx=ctx)
    # Trainer is for updating parameters with gradient.
    trainer = gluon.Trainer(net.collect_params(), 'adam',
                            {'learning_rate': learning_rate},
                            kvstore=kvstore)
    metric = mx.metric.Accuracy()
    loss = gluon.loss.SoftmaxCrossEntropyLoss()
    
    train_confusion_matrices = []
    val_confusion_matrices = []
    
    for epoch in range(epochs):
        # reset data iterator and metric at begining of epoch.
        metric.reset()
        
        btic = time.time()
        i = 0
        
        for batch in train_iterator:
           
            # Copy data to ctx if necessary
            data = batch.data[0].as_in_context(ctx)
            label = batch.label[0].as_in_context(ctx)
            
            # Start recording computation graph with record() section.
            # Recorded graphs can then be differentiated with backward.
            with autograd.record():
                output = net(data)
                L = loss(output, label)
                L.backward()
            # take a gradient step with batch_size equal to data.shape[0]
            trainer.step(data.shape[0])
            # update metric at last.
            metric.update([label], [output])

            if i % log_interval == 0 and i > 0:
                name, acc = metric.get()
                print('[Epoch %d Batch %d] Training: %s=%f, %f samples/s' %
                      (epoch, i, name, acc, batch_size / (time.time() - btic)))

            btic = time.time()
            i += 1

        name, acc = metric.get()
        print('[Epoch %d] Training: %s=%f' % (epoch, name, acc))

        name, val_acc = test(ctx, net, val_iterator)
        print('[Epoch %d] Validation: %s=%f' % (epoch, name, val_acc))
        train_iterator.reset()
        val_iterator.reset()
        ### Adding a step to calculate the confusion matrix 
        train_outputs = []
        train_labels = []
        
        val_outputs = []
        val_labels = []
        
        for group in [[train_outputs, train_labels, train_confusion_matrices, train_iterator], 
                      [val_outputs, val_labels, val_confusion_matrices, val_iterator]]:
                for batch in group[3]:
                    # Copy data to ctx if necessary
                    data = batch.data[0].as_in_context(ctx)
                    label = batch.label[0].as_in_context(ctx)
            
                    output = net(data)
                    group[0].append(output.asnumpy()) #outputs
                    group[1].append(label.asnumpy()) #labels
                group[0] = np.array(group[0])
                group[1] = np.array(group[1])
                group[0] = np.reshape(group[0], (group[0].shape[0] * group[0].shape[1], group[0].shape[2]))
                group[0] = np.argmax(group[0], axis=1)
                group[1] = np.reshape(group[1], (group[1].shape[0] * group[1].shape[1]))
                cm = confusionmatrix(group[1].tolist(), group[0].tolist())
                group[2].append(cm)
        
                group[3].reset()
    
    
    train_cm = np.array(train_confusion_matrices)
    val_cm = np.array(val_confusion_matrices)
    np.save("train_matrix_logs", train_cm)
    np.save("val_matrix_logs", val_cm)
    
    s3.meta.client.upload_file("train_matrix_logs.npy", "sagemaker-us-east-1-602481871371", 'train_matrix_logs.npy')
    s3.meta.client.upload_file("val_matrix_logs.npy", "sagemaker-us-east-1-602481871371", 'val_matrix_logs.npy')
        
    return net


def get_dataset(filename, ctx):
    s3 = boto3.resource('s3')
    if os.path.isfile(filename):
        print("The %s is already here." % filename)
    else:
        print("Fetching %s." % filename)
        s3.meta.client.download_file("novetta-quote-extract",
            "sentences/" + filename, filename)
    with open(filename, 'rb') as f:
        ret = pickle.load(f).as_in_context(ctx)
    return ret[:310000], ret[310000:]


def vocab_to_json(vocab, path):
    with open(path, "w") as out:
        json.dump(vocab, out, indent=4, ensure_ascii=False)
        print('Vocabulary saved to "%s"', path)


def vocab_from_json(path):
    with open(path) as inp:
        vocab = json.load(inp)
        print('Vocabulary (%d words) loaded from "%s"', len(vocab), path)
        return vocab


def save(net, model_dir):
    # save the model
    y = net(mx.sym.var('data'))
    y.save('%s/model.json' % model_dir)
    net.collect_params().save('%s/model.params' % model_dir)


def test(ctx, net, val_data):
    val_data.reset()
    metric = mx.metric.Accuracy()
    for batch in val_data:
        data = batch.data[0].as_in_context(ctx)
        label = batch.label[0].as_in_context(ctx)
        output = net(data)
        metric.update([label], [output])
    return metric.get()


# ------------------------------------------------------------ #
# Hosting methods                                              #
# ------------------------------------------------------------ #

def model_fn(model_dir):
    """
    Load the gluon model. Called once when hosting service starts.

    :param: model_dir The directory where model files are stored.
    :return: a model (in this case a Gluon network)
    """
    symbol = mx.sym.load('%s/model.json' % model_dir)
    vocab = vocab_from_json('%s/vocab.json' % model_dir)
    outputs = mx.symbol.softmax(data=symbol, name='softmax_label')
    inputs = mx.sym.var('data')
    param_dict = gluon.ParameterDict('model_')
    net = gluon.SymbolBlock(outputs, inputs, param_dict)
    net.load_params('%s/model.params' % model_dir, ctx=mx.cpu())
    return net, vocab


def transform_fn(net, data, input_content_type, output_content_type):
    """
    Transform a request using the Gluon model. Called once per request.

    :param net: The Gluon model.
    :param data: The request payload.
    :param input_content_type: The request content type.
    :param output_content_type: The (desired) response content type.
    :return: response payload and content type.
    """
    # we can use content types to vary input/output handling, but
    # here we just assume json for both
    net, vocab = net
    parsed = json.loads(data)
    outputs = []
    for row in parsed:
        tokens = [vocab.get(token, 1) for token in row.split()]
        nda = mx.nd.array([tokens])
        output = net(nda)
        prediction = mx.nd.argmax(output, axis=1)
        outputs.append(int(prediction.asscalar()))
    response_body = json.dumps(outputs)
    return response_body, output_content_type

