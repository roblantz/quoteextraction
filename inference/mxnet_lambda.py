import mxnet as mx
import os
import boto3
from mxnet.contrib.text import embedding 
from mxnet.io import NDArrayIter
from mxnet import gluon

import re
import json
from time import time
import pickle
import tempfile
from collections import namedtuple
import numpy as np

start = time()

### Helpers to define the location of data in S3 ###
BUCKET = "novetta-quote-extract"
DIR = "intermediate_models/"
s3 = boto3.resource("s3")

### With MXNet 1.1 installed, this file is available via the helper object below ###
EMBED_FILE = "glove.6B.50d.txt"

SENT_LEN = 25

### The current model parameters file which defines the model weights ###
f_params = "model_intermediate_epoch-5.params"

Batch = namedtuple('Batch', ['data'])
ctx = mx.cpu()

### Load the model parameters file from S3 ###
f_params_file = tempfile.NamedTemporaryFile()
s3.meta.client.download_file(BUCKET, DIR + f_params, f_params_file.name)
print("Loaded %s from %s%s" % (f_params, BUCKET + "/", DIR))
f_params_file.flush()

print("We're using MXNet v%s." % str(mx.__version__))

def fetch_embedding(fname):
	"""
	If using your own container the embedding fetch is not necessary,
	however, the vocabulary embedding will be necessary, as that is
	specific to the training corpus.
	`fname` is the EMBED_FILE name.
	Returns: the embedding vocabulary.
	"""
	if os.path.isfile("/tmp/" + fname):
		print("%s already exists" % fname)
	else:
		s3.meta.client.download_file(BUCKET, fname, "/tmp/" + fname)
	if os.path.isfile("/tmp/embed_vocab.p"):
		print("the vocabulary is already present")
	else:
		s3.meta.client.download_file(BUCKET, "embed_vocab.p", "/tmp/embed_vocab.p")
	ret = pickle.load(open("/tmp/embed_vocab.p", "rb"))
	print("Successfully accessed embeddings and vocab.")
	return ret


def load_model(p_fname):
	"""
	We explicitly define the name scope in order to avoid dealing with the lack of 
	support for Gluon model dumps experienced in training (this may not be an issue 
	training with a MXNet 1.1 training instance).  This way we only need the .params file.

	`p_fname` is the .params file.
	Returns: the model object.
	"""
	net = gluon.nn.HybridSequential()
	with net.name_scope():
		net.add(gluon.nn.Conv1D(channels = 64, kernel_size = 3, activation = 'relu', padding = 3))
		net.add(gluon.nn.MaxPool1D(3))
		net.add(gluon.nn.BatchNorm())
		net.add(gluon.nn.Conv1D(channels = 128, kernel_size = 5, activation = 'relu', padding = 4))
		net.add(gluon.nn.MaxPool1D(5))
		net.add(gluon.nn.BatchNorm())
		net.add(gluon.nn.Conv1D(channels = 256, kernel_size = 7, activation = 'relu', padding = 5))
		net.add(gluon.nn.MaxPool1D(7))
		net.add(gluon.nn.BatchNorm())
		net.add(gluon.nn.Conv1D(channels = 128, kernel_size = 5, activation = 'relu', padding = 4))
		net.add(gluon.nn.MaxPool1D(5))
		net.add(gluon.nn.BatchNorm())
		net.add(gluon.nn.Conv1D(channels = 64, kernel_size = 5, activation = 'relu', padding = 4))
		net.add(gluon.nn.MaxPool1D(5))
		net.add(gluon.nn.BatchNorm())
		net.add(gluon.nn.Conv1D(channels = 32, kernel_size = 3, activation = 'relu', padding = 3))
		net.add(gluon.nn.MaxPool1D(3))
		#net.add(gluon.rnn.LSTM(3, 2)) ### Added this in hopes that it would, you know, just work.
		net.add(gluon.nn.BatchNorm())
		net.add(gluon.nn.Flatten())
		net.add(gluon.nn.Dense(75, activation = 'relu'))
		net.add(gluon.nn.Dropout(0.5))
		net.add(gluon.nn.Dense(2, activation = 'softrelu'))
	net.load_params(p_fname, ctx = ctx)
	return net


def embed_sent(sent, mat):
	"""
	`sent` is the list of words.
	`mat` is the embedding_matrix.
	Returns: an array of word vectors.
	"""
	ret = mat.get_vecs_by_tokens(sent).reshape((1, 25, 50))
	return ret


def construct_sent(sent):
	"""
	Reconstruct the sentence token by token.
	Returns: a string of the sentence.
	"""
	sentence = " "
	space_after = [r'.', r',', r'?', r'!', r':', r';', r'%', r"''", r")"]
	space_before = ["(", "``"]
	for word in sent:
		if word in space_after:
			sentence = sentence[:-1] + word + " "
		elif word in space_before:
			sentence = sentence + word
		elif len(word) > 1 and (word[0] == "'" or word[1] == "'"):
			if word[0] == "'" or word[1] == "'":
				sentence = sentence[:-1] + word + " "
		elif word == '<unk>':
			break
		else:
			sentence = sentence + word + " "
	return sentence


def reconstruct(res):
	"""
	Reconstruct the article sentence by sentence adding attrib tags where
	appropriate.
	Returns: a string of the tagged article.
	"""
	tagged = r''
	start = r'<attrib>'
	end = r"</attrib>"

	for sent in res:
		if sent[0] == 1:
			tagged = tagged + " " + start + " "
			tagged = tagged + construct_sent(sent[1:])
			tagged = tagged + " " + end + " "
		else:
			tagged = tagged + construct_sent(sent[1:])

	return tagged


def predict(data, net):
	"""
	`data` is a sentence represented as an array of embedded vectors.
	`net` is the model object.
	Returns: label of 1 or 0 for attrib or not.
	"""
	preds = net(data).asnumpy()
	preds = np.squeeze(preds)
	preds = np.argsort(preds)
	return int(preds[1])


def lambda_handler(event, context):
	"""
	This method can be replaced with 'main'
		`event` is a dictionary object that contains file metadata
		`context` is not used here.
	Returns: list of labels and sentence tokens, which is not important
	"""

	### Parsing the source file location and name. ###
	art_file_src = event['Records'][0]['s3']['object']['key']
	bucket = event['Records'][0]['s3']['bucket']['name']
	sep = art_file_src.find("/") + 1
	src = art_file_src[:sep]
	json_file = art_file_src[sep:]

	print("%s is the json of sentences." % json_file)
	print("%s is the src directory" % src)

	### The final S3 location of the output. ###
	dest = 'test-output/'

	### Get the list of lists that was the result of the preprocessing step ###
	s3.meta.client.download_file(bucket, src + json_file, "/tmp/" + json_file)
	print("successful download")

	# art is a list (sentences) of lists (words) 
	art = json.load(open("/tmp/" + json_file, "r")) 
	results = []

	### Load the model ###
	net = load_model(f_params_file.name)
	print("loaded the model")

	### Get the vocabular, and, in this version, download the embedding matrix file ###
	vocab = fetch_embedding(EMBED_FILE)
	print("fetched embedding")
	embedding_matrix = embedding.CustomEmbedding(pretrained_file_path = "/tmp/" + EMBED_FILE, vocabulary = vocab)
	print("created embedding matrix")

	### Create list of labels and sentence tokens to be reconstructed ###
	model_output = []
	for sent in art:
		input_data = embed_sent(sent, embedding_matrix)
		output = predict(input_data, net)
		hold = [output] + sent
		model_output.append(hold)

	### Text blob of the reconstructed article ###
	blob = reconstruct(model_output)
	
	### Upload the result to S3 ###
	json.dump(blob, open("/tmp/tagged-art.json", "w"))
	s3.meta.client.upload_file("/tmp/tagged-art.json", bucket, dest + json_file)
	print("Uploaded tagged-art.json to %s at %s." % (dest, bucket))
	return model_output
