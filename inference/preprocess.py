import imp
import sys
import os
from time import time

sys.modules["sqlite"] = imp.new_module("sqlite")
sys.modules["sqlite3.dbapi2"] = imp.new_module("sqlite.dbapi2")
sys.path.append(".")

### This try/except block is not necessary outside of Lambda ###
try:
	os.mkdir("./nltk_data")
	print("Created directory")
except:
	print("Directory already exists.")

import nltk

import boto3
import json

### Only required if you pull data from AWS S3 ###
s3 = boto3.resource('s3')
SENT_LEN = 25

def pad_sents(sent):
	"""
	Expects `sent` to be a list of words comprising a sentence.

	Returns: list of lower case words padded to SENT_LEN.
	"""
	low = []
	if len(sent) < SENT_LEN:
		sent = sent + ["<unk>"] * (SENT_LEN - len(sent))
	elif len(sent) > SENT_LEN:
		sent = sent[:SENT_LEN]
	else:
		sent = sent

	for s in sent:
		low.append(s.lower())

	return low


def lambda_handler(event, context):
	"""
	This method can be replaced with 'main'
		`event` is a dictionary object that contains file metadata
		`context` is not used here.
	Returns: None
	"""

	### A bunch of parsing to get the file name and source directory in S3. ###
	art_file_src = event['Records'][0]['s3']['object']['key']
	bucket = event['Records'][0]['s3']['bucket']['name']
	sep = art_file_src.find("/") + 1
	src = art_file_src[:sep]
	art_file = art_file_src[sep:]
	art_name = art_file.replace(".", "") + "-" + str(time()).replace(".", "")
	print("%s is the art_file." % art_file)
	print("%s is the src directory" % src)

	### Defines where the file will be uploaded to on S3 ###
	dest = 'test-results/'

	### Uses the resource object to download the file locally ###
	s3.meta.client.download_file(bucket, src + art_file, "/tmp/" + art_file)
	print("successful download")
	with open("/tmp/" + art_file, "r") as f:
		blob = f.read()
	print("successful read")

	### Uses the standard nltk tokenizer to break articles into sentences ###
	sents = nltk.sent_tokenize(blob)
	print("successful sent tokenize")
	padded_sents = []

	### Creates the list (article sentences) of list (sentence words) structure ###
	for sent in sents:
		toks = nltk.word_tokenize(sent)
		padded_sents.append(pad_sents(toks))
	print("successful word tokenize")

	### Dump and upload the file ###
	with open('/tmp/result.json', 'w') as res:
		json.dump(padded_sents, res)
	print("successful dump")
	s3.meta.client.upload_file('/tmp/result.json', bucket, dest + art_name + '.json')
	print("successful upload")
	
	return
