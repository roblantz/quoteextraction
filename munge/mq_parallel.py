# -*- coding: utf-8 -*-
"""
Created on Tue Sep 19 06:08:44 2017

@author: rlantz
"""

import multiprocessing as mp
import boto3

import difflib
import json
import pandas as pd
from io import StringIO

s3_resource = boto3.resource('s3')
client = boto3.client('s3')
bucket = s3_resource.Bucket('novetta-quote-extract')

s3 = boto3.client('s3')
sent_list = []
cpus = mp.cpu_count() - 1
bucket_name = 'novetta-quote-extract'
folder = 'sentences'

sentences = '53685.csv'
key = folder + '/' + sentences
obj = s3.get_object(Bucket = bucket_name, Key = key)
body = obj['Body']
csv_string = body.read().decode('utf-8')
sents = pd.read_csv(StringIO(csv_string), delimiter = "|")

quotes = 'quote_out.csv'
key = folder + '/' + quotes
obj = s3.get_object(Bucket = bucket_name, Key = key)
body = obj['Body']
csv_string = body.read().decode('utf-8')
qts = pd.read_csv(StringIO(csv_string), delimiter = "|")

artIDs = sents['articleID'].unique()
df_list = []
for artID in artIDs:
    sent_hold = sents[['articleID', 'sentence', 'start', 'stop']][sents.articleID == artID].reset_index()
    quote_hold = qts[['Article ID', 'Quote Text', 'start', 'stop']][qts['Article ID'] == artID].reset_index()
    df_list.append((sent_hold, quote_hold))

print("Done here.")
def is_in(num, start, stop):
    ret = False
    if num >= start and num <= stop:
        ret = True
    return ret

def get_overlap(s1, s2):
    s = difflib.SequenceMatcher(None, s1, s2)
    pos_a, pos_b, size = s.find_longest_match(0, len(s1), 0, len(s2))
    return max(float(size)/len(s1),float(size)/len(s2))

def find_matches(tupe):
    ss = tupe[0]
    qs = tupe[1]
    ret = []
    
    for s in ss.iterrows():
        i1 = s[1].start
        i2 = s[1].stop
        artID = s[1].articleID
        sent = s[1].sentence
        qInd = 0
        if pd.isnull(sent):
            pass
        else:
            for q in qs.iterrows():
                j1 = q[1].start
                j2 = q[1].stop
                qte = q[1]['Quote Text']
                if pd.isnull(qte):
                    pass
                else:
                    if get_overlap(sent, qte) > 0.4:
                        qInd = 1
                        break
            ret.append((artID, sent, qInd, i1, i2))    
    return ret  

def mp_handler():
    p = mp.Pool(cpus)
    return p.map(find_matches, df_list)
    
if __name__ == '__main__':
    output = mp_handler()
    
outfile_name = sentences[:-4] + '_coded.json'
outfile_json = open(outfile_name, 'w')
json.dump(output, outfile_json)
outfile_json.close()
outfile = open(outfile_name, 'rb')
s3_resource.Object(bucket_name, folder + '/' + outfile_name).put(outfile)
outfile.close()
