# -*- coding: utf-8 -*-
"""
Created on Tue Sep 12 06:00:46 2017

@author: rlantz
@resource https://stackoverflow.com/questions/35803601/reading-a-file-from-a-private-s3-bucket-to-a-pandas-dataframe
"""

import boto3
import codecs
#import chardet
import csv
from io import StringIO
import re

s3 = boto3.resource('s3')
client = boto3.client('s3')
bucket = s3.Bucket('novetta-quote-extract')
folder = 'sentences/'
file_name = 'artText.csv'
regex = '(?<=\.|\?|"|\!)\s+(?=[A-Z]|")'

write_file = StringIO()
write_file.write('articleID|sentence|start|stop\n')
object_body = bucket.Object(folder + file_name).get()['Body']

for i, row in enumerate(csv.DictReader(codecs.getreader('utf-8')(object_body, errors = 'replace'))):
    if i % 10000 == 0:
        print("I'm at: " + str(i))
        if i > 0:
            write_file.seek(0)
            outfile_name = folder + str(i) + '.csv'
            client.put_object(Body = write_file.read(), Bucket = 'novetta-quote-extract', Key = outfile_name)
            write_file.close()
            write_file = StringIO()
            write_file.write('articleID|sentence|start|stop\n')

    art = row['articleText']
    art = art.replace('\n', ' ').replace('\r', ' ').replace('|', ' ')
    artID = int(float(row['articleID']))

    sent_list = re.split(regex, art)
    for s in sent_list:
        start = art.find(s)
        stop = start + len(s)
        write_sent = str(artID) + "|" + s + "|" + str(start) + "|" + str(stop) + '\n'
        #write_sent = write_sent.encode('ascii')
        write_file.write(write_sent)

write_file.seek(0)
client.put_object(Body = write_file.read(), Bucket = 'novetta-quote-extract', Key = folder + str(i) + '.csv')   
