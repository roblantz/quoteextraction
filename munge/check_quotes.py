# -*- coding: utf-8 -*-
"""
Created on Thu Sep 14 15:33:44 2017

@author: rlantz
"""
import os
import csv
from datetime import datetime
os.chdir("C://Users//rlantz//Desktop//Quotes")

fact_reader = csv.reader(open('Fact56-export.csv', 'rU'), delimiter = '|')
output = open('quote_out.csv', 'w')
cnt = 0
output.write('date|Article ID|start|stop|Quote Text\n')

for i, fact in enumerate(fact_reader):
    try:
        f0 = datetime.strptime(fact[0], '%Y-%m-%d')
        f1 = fact[3].replace(',', '')
        if str(f1) == '200200342348':
            print("I'm here")
            print(f0)
        f2 = fact[9]
        f3 = fact[12]
        f4 = fact[15].replace('\n', ' ').replace('\r', ' ').replace("  ", " ").replace("|", " ")
        if f0 >= datetime(2014, 01, 01) and f0 <= datetime(2016, 10, 05):
            output.write(f0.strftime('%m/%d/%Y') + "|" + f1 + '|' + f2 + '|' + f3 + '|' + f4 + '\n')
    except:
        cnt += 1
        
print("There were %d failures." % cnt)
output.close()